import {Link, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import config from "./config";

function UserDetails(props) {
    let {id} = useParams();

    const [user, setUser] = useState(null);

    useEffect(() => {
        fetch("https://gorest.co.in/public/v2/users/" + id + "?access-token=" + config.apiKey)
            .then(response => response.json())
            .then(data => setUser(data));
    }, [id]);

    return (
        <div>
            <h1>User details</h1>
            {user ? <div>
                <p>{user.id}</p>
                <p>{user.name}</p>
                <p>{user.email}</p>
            </div> : <p>Loading....</p>}

            <Link to={"/"}>Back</Link>
        </div>
    );
}

export default UserDetails;