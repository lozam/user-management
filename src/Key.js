import config from "./config";
import {useState} from "react";

function Key() {
    let [key, updateKey] = useState(config.apiKey ? config.apiKey : "")
    const saveKey = () => {
        localStorage.setItem("apiKey", key);
        config.apiKey = key;
    }
    return (<div>
        <input type={"text"} value={key} onChange={(e) => updateKey(e.target.value)}/>
        <button onClick={saveKey}>Save</button>
    </div>)
}

export default Key;