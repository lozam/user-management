import './App.css';
import {useEffect, useState} from "react";
import config from "./config";
import CreateUser from "./CreateUser";
import {BrowserRouter, Link, Navigate, Outlet, Route, Routes} from "react-router-dom";
import UserDetails from "./UserDetails";
import Key from "./Key";

function User({user, refresh}) {
    const deleteUser = () => {
        fetch("https://gorest.co.in/public/v2/users/" + user.id + "?access-token=" + config.apiKey,
            {
                method: "DELETE"
            })
            .then(() => refresh());
    }

    return <tr key={user.id}>
        <td>{user.id}</td>
        <td><Link to={`/details/${user.id}`}>{user.name}</Link></td>
        <td>{user.email}</td>
        <td>
            <button onClick={() => deleteUser()}>
                Delete
            </button>
        </td>
    </tr>
}

function App() {

    const [users, setUsers] = useState([]);

    const refresh = () => {
        fetch("https://gorest.co.in/public/v2/users?email=4t.com&access-token=" + config.apiKey)
            .then(response => {
                if(response.status === 200) {
                    return response.json()
                }

                throw new Error("Nie udało się pobrać użytkowników");
            })
            .then(data => setUsers(data))
            .catch(() => setUsers([]));
    }

    useEffect(() => {
        refresh();
    }, [])

    console.log(users);

    return (
        <div className="App">
            <h1>Zarządzanie użytkownikami!!!</h1>
            <BrowserRouter>
                <Routes>
                    <Route path={"/key"} element={<Key/>}/>
                    <Route path={"/"} element={config.apiKey ? (<Outlet/>) : <Navigate to={"/key"}/>}>
                        <Route path={"/"} element={(
                            <>
                                <table>
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <td>Username</td>
                                        <td>Email</td>
                                        <td>Akcja</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {users.map((user) => <User
                                        key={user.id}
                                        user={user}
                                        refresh={refresh}/>)}
                                    </tbody>
                                </table>

                                <CreateUser refresh={refresh}/>
                            </>
                        )}/>
                        <Route path="/details/:id" element={<UserDetails/>}/>
                    </Route>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
