import {useState} from "react";
import config from "./config";

function CreateUser(props) {

    let [user, setUser] = useState(
        {email: "", name: "4T User!", gender: "male", status: "active"});

    const saveUser = () => {
        fetch("https://gorest.co.in/public/v2/users?access-token=" + config.apiKey,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(user)
            }).then(r => {
                props.refresh();
                setUser({...user, email: ""});
        })
    }

    return <>
        <input
            type="email"
            value={user.email}
            onChange={(e) => {
                setUser({...user, email: e.target.value});
            }}/>

        <button onClick={() => saveUser()}>Zapisz</button>
    </>
}

export default CreateUser;